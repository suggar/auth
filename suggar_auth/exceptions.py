"""Contains exceptions related to auth process."""


class AuthError(Exception):
    """
    Raised if any auth error happens.

    This is base error for every error in auth package.
    """

    message = ''

    def __init__(self, message='Unable to authenticate'):
        """Initialize exception."""
        Exception.__init__(self, message)
        self.message = message

    def __str__(self):
        """Make string representation of exception."""
        return self.message


class InvalidAuthTypeError(AuthError):
    """Raised when user attempted to authorize via unknown token provider."""

    auth_type = None

    def __init__(self, message='Unknown auth type', auth_type=None):
        """Initialize exception."""
        AuthError.__init__(self, message)
        self.auth_type = auth_type


class InvalidCredentialsError(AuthError):
    """Raised when user gave invalid credentials."""

    auth_type = None
    auth_data = None

    def __init__(
            self, message='Invalid credentials',
            auth_type=None, auth_data=None):
        """Initialize exception."""
        AuthError.__init__(self, message)
        self.auth_type = auth_type
        self.auth_data = auth_data


class InvalidLocalTokenError(AuthError):
    """Raised when user gave invalid local token during authorization."""

    local_token = None

    def __init__(self, message='Invalid local token', local_token=None):
        """Initialize exception."""
        AuthError.__init__(self, message)
        InvalidLocalTokenError.local_token = local_token
