"""Contains core things to provide auth system."""
import base64
import binascii
import hashlib

from suggar_auth import exceptions as auth_exceptions
from suggar_auth.exceptions import InvalidCredentialsError
from suggar_auth.exceptions import InvalidLocalTokenError


class TokenManager:
    """
    An interface that is used to maintain providers' tokens.

    Should encapsulate all logic somehow related to token lifecycle.
    """

    def verify_payload(self, payload):
        """
        Check whether payload is valid.

        Return remote_user_id that corresponds to payload
        :rtype: str
        """
        raise NotImplementedError('Abstract class method called')

    def get_user_external_token(self, internal_id):
        """
        Retrieve user's external access token.

        :param internal_id: user id
        :return: access token to external service
        """
        raise NotImplementedError('Abstract class method called')

    def get_user_external_id(self, internal_id):
        """
        Retrieve user's external id.

        :param internal_id: user's internal id
        :return: user's id in external service
        """
        raise NotImplementedError('Abstract class method called')


class UserManager:
    """
    An interface that provide user objects lifecycle.

    This includes user creation, by-id retrieval and update operations.
    """

    def search_by_external_id(self, external_id):
        """
        Retrieve user by his external id.

        :param external_id: User's id in external service
        :return: user object (dict)
        :rtype: dict
        """
        raise NotImplementedError('Abstract class method called')

    def create_user(self, username, meta=None):
        """
        Create user with provided username and meta info.

        :param username: unique name for user
        :param meta: dict with any info to store
        :return: user object (dict)
        :rtype: dict
        """
        raise NotImplementedError('Abstract class method called')

    def attach_external_credentials(
            self, internal_id, external_id, auth_type, payload):
        """
        Attach credentials that is in payload list to specified user with.

        :param internal_id: internal user id
        :param auth_type: token provider name actually
        :param payload: user's external credentials to attach
        """
        raise NotImplementedError('Abstract class method called')

    def update_external_credentials(self, external_id, auth_type,
                                    payload_list):
        """
        Update user's external credentials.

        :param external_id: user's identifier in external service
        :param auth_type: external service name (e. g. Spotify)
        :param payload_list: list with user's credentials
        """
        raise NotImplementedError('Abstract class method called')


class SessionManager:
    """An interface for session storage classes."""

    def load_user(self):
        """
        Load user object from session.

        :return: user object
        """
        raise NotImplementedError('Abstract class method called')

    def save_user(self, user_object):
        """
        Save user object to session.

        :param user_object: user object to save
        """
        raise NotImplementedError('Abstract class method called')


class CredentialsManager:
    """Provides authentication and authorization logic."""

    def __init__(self, secret, user_manager=None, session_manager=None):
        """
        Initialize CredentialManager instance.

        :param secret: server secret string to use in local tokens
        """
        self.secret = secret
        self.token_managers = {}
        self.user_manager = user_manager
        self.session_manager = None  # type: SessionManager

    def obtain_local_token(self, auth_type, auth_data):
        """
        Obtain local token via specified TokenProvider.

        This token could be used to authorize user to do something.
        User manager is used to store/retrieve user.
        If the token is valid, but there is no such user in system,
        the new user will be created and current token will be attached.
        :param auth_type: TokenManager name (should be registered first)
        :param auth_data: base64-encoded payload
        :return: local token

        :raises InvalidAuthTypeError
        :raises InvalidCredentialsError

        :rtype str
        """
        try:
            token_manager = self.token_managers[auth_type]
        except KeyError:
            raise auth_exceptions.InvalidAuthTypeError(auth_type=auth_type)

        try:
            payload_list = self._decode(auth_data).split(' ')
        except UnicodeDecodeError:
            raise InvalidCredentialsError()
        except binascii.Error:
            raise InvalidCredentialsError()

        external_id = token_manager.verify_payload(payload_list)
        external_id = str(external_id)
        if not external_id:
            raise InvalidCredentialsError()

        internal_id = self._retrieve_internal_id(
            external_id, auth_type, payload_list)
        internal_id = str(internal_id)

        self.user_manager.update_external_credentials(
            external_id, auth_type, payload_list)

        return self._generate_local_token(internal_id)

    def register_token_manager(self, target_auth_type, token_manager):
        """
        Register TokenManager instance.

        :param token_manager: instance to register
        :param target_auth_type:
        """
        self.token_managers[target_auth_type] = token_manager

    def register_token_manager_factory(self, target_auth_type):
        """
        Register TokenManager instance created by decorated factory.

        The factory itself left untouched.
        :param target_auth_type: Name of the TokenProvider.
        """
        def registerer_decorator(token_manager_factory):
            # Yeah we construct token_manager right here
            self.register_token_manager(
                target_auth_type, token_manager_factory())

            # And then the factory remains the same
            return token_manager_factory

        return registerer_decorator

    def auth_user(self, local_token):
        """
        Try to authenticate user with provided local_token.

        If not succeeded - throws exception. Otherwise
        stores user_id via SessionStorage instance

        :param local_token: The local token itself

        :raises InvalidLocalTokenError
        """
        payload, signature = self._check_local_token(local_token)
        internal_id = payload[0]
        self.session_manager.save_user(internal_id)

    def _retrieve_internal_id(self, external_id, auth_type, payload):
        """
        Get internal id by id under which user known in some external service.

        Warning! If user doesn't exists, it will be created and then returned
        :param external_id: user id in some external service
        :param auth_type: external service name
        :param payload: some credentials in case storage is required
        :return: user's internal id
        """
        internal_user = self.user_manager.search_by_external_id(external_id)
        if not internal_user:
            internal_user = self.user_manager.create_user(
                username=external_id)
            self.user_manager.attach_external_credentials(
                internal_user['id'], external_id, auth_type, payload)

        return internal_user['id']

    def _check_local_token(self, local_token):
        """
        Check if local_token is valid.

        Decode local_token, check whether it is valid one
        and return its payload and signature.

        :param local_token: The local token itself
        :return: unpacked payload and signature
        :raises InvalidLocalTokenError
        :rtype Tuple[List[str], str]
        """
        try:
            raw_payload = self._decode(local_token)
        except UnicodeDecodeError:
            raise InvalidLocalTokenError(local_token=local_token)
        except binascii.Error:
            raise InvalidLocalTokenError(local_token=local_token)

        raw_payload_list = raw_payload.split(' ')

        payload = raw_payload_list[:-1]
        signature = raw_payload_list[-1:][0]

        user_id = payload[0]
        expected_signature = self._hash(user_id + self.secret)
        if signature != expected_signature:
            raise InvalidLocalTokenError(local_token=local_token)

        return payload, signature

    def _generate_local_token(self, user_id):
        """
        Generate token for specified user.

        :param user_id:
        :return: valid local token that can be used authenticate
        """
        signature = self._hash(user_id + self.secret)
        payload = [
            user_id]
        payload_str = ' '.join(payload)
        prepared_str = ' '.join((payload_str, signature))

        return self._encode(prepared_str)

    def _encode(self, src):
        b_str = bytes(src, 'utf-8')
        return base64.urlsafe_b64encode(b_str).decode('utf-8')

    def _decode(self, src):
        b_str = bytes(src, 'utf-8')
        return base64.urlsafe_b64decode(b_str).decode('utf-8')

    def _hash(self, src):
        prepared_src = bytes(src, 'utf-8')
        hash_object = hashlib.sha512(prepared_src)
        return hash_object.hexdigest()
