"""Settings for proper package installation."""
from setuptools import setup

setup(
    name='suggar_auth',
    version='0.1.0',
    description='Authorization processes core',
    url='https://gitlab.com/suggar/auth',
    author='Nef1k',
    author_email='nef1k@outlook.com',
    license='MIT',
    packages=[
        'suggar_auth'],
    zip_safe=False)
